import { createApp } from 'vue'
import axios from 'axios';
import App from './App.vue';
createApp(App,axios).mount('#app')
